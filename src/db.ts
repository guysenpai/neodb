/*!
 * NeoDB - Database
 * Copyright (c) 2017 Guy Senpai <guy.senpai@gmail.com>
 * MIT Licensed
 */
"use strict";

import merge = require('lodash.merge');
import { join } from 'path';
import { format as f } from 'util';

import { Collection } from './collection';
import { Storage } from './storage';

/**
 * Database
 * @class
 */
class DB {

  /** @ignore */
  databaseName: string;

  /** @ignore */
  databasePath: string;

  /** @ignore */
  dbCache: any;

  /** @ignore */
  children: DB[];

  /** @ignore */
  options: any;

  /** @ignore */
  parentDb: DB;

  /** @ignore */
  private _collections: { [collection: string]: Collection };

  /**
   * @constructor
   * @param {String} name
   * @param {Object} [options]
   * @param {Boolean} [options.autoload=true]
   * @param {Boolean} [options.autocreate=true]
   * @throws
   * @description Creates a new DB instance
   */
  constructor(name: string, options?: any) {
    options = options || {};
    if (!(this instanceof DB)) return new DB(name, options);

    this._collections = {};
    this.options = merge({ autocreate: true, autoload: true }, options);
    this.databaseName = name;
    this.databasePath = join('.data', this.databaseName);

    // Check if database name is an url
    // then overwrite databasePath
    if (name.includes('/')) {
      const arr = name.split('/');
      this.databaseName = arr[arr.length - 1];
      this.databasePath = name;
    }

    // Check if database exists
    // If not create new database folder
    if (Storage.isValidPath(this.databasePath)) {
      // Load all collections if necessary
      if (this.options.autoload) {
        this.loadCollections();
      }
    } else {
      if (this.options.autocreate) {
        Storage.newFolder(this.databasePath);
      } else {
        throw new Error(f("Database path %s does not seem to be valid. Recheck the url and try again", this.databasePath));
      }
    }
  }


  /**
   * @static
   * @method
   * @param {String} url
   * @param {Object} [options]
   * @param {Boolean} [options.autoload]
   * @param {DB~ConnectCallback} [callback]
   * @returns {Promise}
   */
  static connect(url: string, options: any, callback: ConnectCallback);
  static connect(url: string, callback: ConnectCallback);

  static connect(url: string, options?: any): Promise<DB>;

  static connect(url: string, options?: any, callback?: ConnectCallback): Promise<DB> | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};

    try {
      const db = new DB(url, options);
      if (typeof callback === 'function') return callback(null, db);
      return new Promise((resolve, reject) => resolve(db));
    } catch (err) {
      if (typeof callback === 'function') return callback(err);
      return new Promise((resolve, reject) => reject(err));
    }
  }


  /**
   * @method
   * @param {String} name
   * @param {Object} [options]
   * @param {Boolean} [options.strict]
   * @param {DB~CollectionResultCallback} [callback]
   * @returns {Collection}
   */
  collection(name: string, options: any, callback: CollectionResultCallback);
  collection(name: string, callback: CollectionResultCallback);

  collection(name: string, options?: any): Collection;

  collection(name: string, options?: any, callback?: CollectionResultCallback): Collection | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};

    // Strict mode
    if (options.strict === true && !(this._collections[name] instanceof Collection)) {
      if (typeof callback === 'function') return callback(new Error(f("Collection %s does not exists. Currently in strict mode", name)));

      throw new Error(f("Collection %s does not exists. Currently in strict mode", name));
    }

    try {
      if (!(this._collections[name] instanceof Collection)) {
        this.loadCollection(name);
      }
    } catch (err) {
      if (typeof callback === 'function') return callback(err);
      throw err;
    }

    if (typeof callback === 'function')  return callback(null, this._collections[name]);

    return this._collections[name];
  }

  /**
   * @method
   * @param {DB~CollectionsResultCallback} [callback]
   * @returns {Promise}
   */
  collections(callback: CollectionsResultCallback);

  collections(): Promise<Collection[]>;

  collections(callback?: CollectionsResultCallback): Promise<Collection[]> | void {
    try {
      this.loadCollections();
      const results = [];

      for (const name in this._collections) {
        results.push(this._collections[name]);
      }

      if (typeof callback === 'function') return callback(null, results);
      return new Promise((resolve, reject) => resolve(results));
    } catch (err) {
      if (typeof callback === 'function') return callback(err);
      return new Promise((resolve, reject) => reject(err));
    }
  }

  /**
   * @method
   * @param {String} name
   * @param {Object} [options]
   * @param {Boolean} [options.strict]
   * @param {DB~CollectionResultCallback} [callback]
   * @returns {Promise}
   */
  createCollection(name: string, options: any, callback: CollectionResultCallback);
  createCollection(name: string, callback: CollectionResultCallback);

  createCollection(name: string, options?: any): Promise<Collection>;

  createCollection(name: string, options?: any, callback?: CollectionResultCallback): Promise<Collection> | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};

    // Strict mode
    if (options.strict === true && this._collections[name] instanceof Collection) {
      if (typeof callback === 'function') return callback(new Error(f("Collection %s already exists. Currently in strict mode", name)));

      return new Promise((resolve, reject) => reject(new Error(f("Collection %s already exists. Currently in strict mode", name))));
    }

    try {
      if (!(this._collections[name] instanceof Collection)) {
        this.loadCollection(name);
      }
    } catch (err) {
      if (typeof callback === 'function') return callback(err);
      return new Promise((resolve, reject) => reject(err));
    }

    if (typeof callback === 'function') return callback(null, this._collections[name]);

    return new Promise((resolve, reject) => resolve(this._collections[name]));
  }

  /**
   * @method
   * @param {String} name
   * @param {DB~ResultCallback} callback
   * @returns {Promise}
   */
  dropCollection(name: string, callback: ResultCallback);

  dropCollection(name: string): Promise<boolean>;

  dropCollection(name: string, callback?: ResultCallback): Promise<boolean> | void {
    const collectionFile = join(this.databasePath, name);

    if (!Storage.isValidPath(collectionFile)) {
      if (typeof callback === 'function') return callback(new Error(f("Collection %s does not exists.", name)));
      return new Promise((resolve, reject) => reject(new Error(f("Collection %s does not exists.", name))));
    }

    if (Storage.isValidPath(collectionFile)) {
      Storage.removeFile(collectionFile);
      this._collections[name] = undefined;
    }

    if (typeof callback === 'function') return callback(null, true);

    return new Promise((resolve, reject) => resolve(true));
  }

  /**
   * @method
   * @param {String} name
   * @param {Object} [options]
   * @param {Boolean} [options.returnNonCachedInstance=false]
   * @returns {DB}
   */
  db(name: string, options?: any): DB {
    options = options || {};

    if (this.dbCache[name] && !options.returnNonCachedInstance) {
      return this.dbCache[name];
    }

    this.parentDb = this;

    const db = new DB(name, this.options);
    this.children.push(db);
    this.dbCache[name] = db;

    return db;
  }

  /**
   * @method
   * @param {DB~ResultCallback} callback
   * @return {Promise}
   */
  dropDatabase(callback: ResultCallback);

  dropDatabase(): Promise<boolean>;

  dropDatabase(callback?: ResultCallback): Promise<boolean> | void {
    if (!Storage.isValidPath(this.databasePath)) {
      if (typeof callback === 'function') return callback(new Error(f("Database %s does not exists.", this.databaseName)));
      return new Promise((resolve, reject) => reject(new Error(f("Database %s does not exists.", this.databaseName))));
    }

    if (Storage.isValidPath(this.databasePath)) {
      Storage.removeFolderRecursive(this.databasePath);
      this._collections = {};
    }

    if (typeof callback === 'function') return callback(null, true);

    return new Promise((resolve, reject) => resolve(true));
  }

  /**
   * @method
   * @param {String} collection
   * @returns {Collection}
   * @description Load the collection with name provided
   */
  loadCollection(collection: string): void {
    if (!collection.includes('.json')) {
      collection = `${collection}.json`;
    }

    const collectionFile = join(this.databasePath, collection);
    // create new collection file if not exists
    if (!Storage.isValidPath(collectionFile)) {
      Storage.writeToFile(collectionFile);
    }

    // add collection instance to collections buffer
    const collectionName = collection.substring(0, collection.indexOf('.json'));

    try {
      this._collections[collectionName] = new Collection(this, collectionName);
    } catch (err) {
      throw err;
    }
  }

  /**
   * @method
   * @param {Array.<String>} [collections]
   * @description Load all collections or with names provided
   */
  loadCollections(collections?: string[]): void {
    if (!this.databasePath) {
      throw new Error("The DB is not initialized. Use DB.connect before load collections.");
    }

    // check if collections array is provided
    if (collections && Array.isArray(collections)) {
      for (let i = 0; i < collections.length; i++) {
        try {
          this.loadCollection(collections[i]);
        } catch (err) {
          throw err;
        }
      }
    }

    // check if collections buffer is empty
    if (Object.keys(this._collections).length === 0) {
      const files = Storage.readFromPath(this.databasePath);

      const collectionsFiles = files.filter((file) => {
        const filename = join(this.databasePath, file);
        return file.includes('.json') && Storage.infoPath(filename).isFile();
      });

      for (let i = 0; i < collectionsFiles.length; i++) {
        try {
          this.loadCollection(collectionsFiles[i]);
        } catch (err) {
          throw err;
        }
      }
    }
  }
}

// export DB class
export { DB };
export { DB as Database };

/**
 * @callback DB~ConnectCallback
 * @param {Error} error
 * @param {DB} db
 */
export type ConnectCallback = (error: Error, db?: DB) => void;

/**
 * @callback DB~CollectionResultCallback
 * @param {Error} error
 * @param {Collection} collection
 */
export type CollectionResultCallback = (error: Error, collection?: Collection) => void;

/**
 * @callback DB~CollectionsResultCallback
 * @param {Error} error
 * @param {Array.<Collections>} collections
 */
export type CollectionsResultCallback = (error: Error, collections?: Collection[]) => void;

/**
 * @callback DB~ResultCallback
 * @param {Error} error
 * @param {Object} result
 */
export type ResultCallback = (error: Error, result?: any) => void;