/*!
 * NeoDB - ObjectID
 * Copyright (c) 2017 Guy Senpai <guy.senpai@gmail.com>
 * MIT Licensed
 */
"use strict";

/**
 * Machine ID
 *
 * Create a random 3-byte value (i.e. unique for this
 * process). Other drivers use a md5 of the machine id here, but
 * that would mean an asyc call to gethostname, so we don't bother.
 * @ignore
 */
const MACHINE_ID = parseInt('' + Math.random() * 0xFFFFFF, 10);

// Regular expression that checks for hex value
const checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
let hasBufferType = false;

// Check if buffer exists
try {
  if (Buffer && Buffer.from) hasBufferType = true;
} catch (err) { }

// Precomputed hex table enables speedy hex string conversion
const hexTable = [];
for (let i = 0; i < 256; i++) {
  hexTable[i] = (i <= 15 ? '0' : '') + i.toString(16);
}

// Lookup tables
const encodeLookup = '0123456789abcdef'.split('');
const decodeLookup = [];
let i = 0;
while (i < 10) decodeLookup[0x30 + i] = i++;
while (i < 16) decodeLookup[0x41 - 10 + i] = decodeLookup[0x61 - 10 + i] = i++;

const convertToHex = (bytes: Buffer): string => bytes.toString('hex');

/**
 * Represents the BSON ObjectID type
 *
 * @class
 * @property {Number} generationTime The generation time of this ObjectId instance.
 */
class ObjectID {

  /** @ignore */
  static index: number = ~~(Math.random() * 0xFFFFFF);

  /** @ignore */
  static cacheHexString: string;

  /** @ignore */
  id: any;

  /** @ignore */
  private __id: string;

  /** @ignore */
  private _bsontype: string;

  /** @ignore */
  get generationTime(): number {
    return this.id[3] | this.id[2] << 8 | this.id[1] << 16 | this.id[0] << 24;
  }

  /** @ignore */
  set generationTime(value: number) {
    this.id[3] = value & 0xff;
    this.id[2] = (value >> 8) & 0xff;
    this.id[1] = (value >> 16) & 0xff;
    this.id[0] = (value >> 24) & 0xff;
  }


  /**
   * Create new ObjectID instance
   * @constructor
   * @param {String|Number} id Can be a 24 byte hex string, 12 byte binary string or a Number.
   * @returns {ObjectID} instance of ObjectID
   */
  constructor(id?: any) {
    // Duck-typing to support ObjectID from differents npm packages
    if (id instanceof ObjectID) return id;
    if (!(this instanceof ObjectID)) return new ObjectID(id);

    this._bsontype = 'ObjectID';

    // The most common usecase (blank id, new ObjectID instance)
    if (id == null || typeof id === 'number') {
      // Generate new id
      this.id = this.generate(id);
      // If we are caching the hex string
      if (ObjectID.cacheHexString) this.__id = this.toString('hex');

      return;
    }

    // Check if the passed in id is valid
    const valid = ObjectID.isValid(id);

    // Throw an error if it's not a valid setup
    if (!valid && id !== null) {
      throw new Error("Argument passed in must be a single String of 12 bytes or a string of 24 hex characters");
    } else if (valid && typeof id === 'string' && id.length === 24 && hasBufferType) {
      return new ObjectID(new Buffer(id, 'hex'));
    } else if (valid && typeof id === 'string' && id.length === 24) {
      return ObjectID.createFromHexString(id);
    } else if (id !== null && id.length === 12) {
      // assume 12 byte string
      this.id = id;
    } else if (id !== null && id.toHexString) {
      // Duck-typing to support ObjectID from differents npm packages
      return id;
    } else {
      throw new Error("Argument passed in must be a single String of 12 bytes or a string of 24 hex characters");
    }

    if (ObjectID.cacheHexString) this.__id = this.toString("hex");
  }


  /** @ignore */
  static createPk(): ObjectID {
    return new ObjectID();
  }

  /**
   * Creates an ObjectID from a hex string representation of an ObjectID.
   *
   * @static
   * @method
   * @param {String} hexString create a ObjectID from a passed in 24 byte hexstring.
   * @returns {ObjectID} the created ObjectID
   */
  static createFromHexString(hexString: string): ObjectID {
    if (typeof hexString === 'undefined' || hexString !== null && hexString.length !== 24) {
      throw new Error("Argument passed in must be a single String of 12 bytes or a string of 24 hex characters");
    }

    // Use Buffer.from method if available3
    if (hasBufferType) return new ObjectID(new Buffer(hexString, 'hex'));

    // Calculate lengths
    const array = new Buffer('12');
    let n = 0;
    let i = 0;

    while (i < 24) {
      array[n++] = decodeLookup[hexString.charCodeAt(i++)] << 4 || decodeLookup[hexString.charCodeAt(i++)];
    }

    return new ObjectID(array);
  }

  /**
   * Creates an ObjectID from a second based number, with the rest of the ObjectID zeroed out. Used for comparisons or sorting the ObjectID.
   *
   * @static
   * @method
   * @param {Number} time an integer number representing a number of seconds.
   * @returns {ObjectID} the created ObjectID
   */
  static createFromTime(time: number): ObjectID {
    const buffer = new Buffer([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
    // Encode time into first 4 bytes
    buffer[3] = time & 0xff;
    buffer[2] = (time >> 8) & 0xff;
    buffer[1] = (time >> 16) & 0xff;
    buffer[0] = (time >> 24) & 0xff;

    return new ObjectID(buffer);
  }

  /**
   * Checks if a value is a valid bson ObjectId
   *
   * @static
   * @method
   * @returns {Boolean} true if the value is a valid bson ObjectId, return false otherwise.
   */
  static isValid(id?: any): boolean {
    if (id == null) return false;

    if (typeof id === 'number') return true;

    if (typeof id === 'string') {
      return id.length === 12 || (id.length === 24 && checkForHexRegExp.test(id));
    }

    if (id instanceof ObjectID) return true;

    if (id instanceof Buffer) return true;

    if (id.toHexString) {
      return id.length === 12 || (id.length === 24 && checkForHexRegExp.test(id.id));
    }

    return false;
  }


  /**
   * Update the ObjectId index used in generating new ObjectId's on the driver.
   *
   * @method
   * @returns {Number} returns next index value.
   * @ignore
   */
  get_inc(): number {
    return ObjectID.index = (ObjectID.index + 1) % 0xFFFFFF;
  }

  /**
   * Update the ObjectId index used in generating new ObjectId's on the driver.
   *
   * @method
   * @returns {Number} returns next index value.
   * @ignore
   */
  getInc(): number {
    return this.get_inc();
  }

  /**
   * Compares the equality of this ObjectID with `otherID`.
   *
   * @method
   * @param {Object} otherID ObjectID instance to compare against.
   * @returns {Boolean} result of comparing two ObjectID's
   */
  equals(otherID: any): boolean {
    const isValid = ObjectID.isValid(otherID);

    if (otherID instanceof ObjectID) {
      return this.toString() === otherID.toString();
    } else if (typeof otherID === 'string' && isValid && otherID.length === 12 && this.id instanceof Buffer) {
      return otherID === this.id.toString('binary');
    } else if (typeof otherID === 'string' && isValid && otherID.length === 24) {
      return (otherID.toLowerCase() === this.toHexString());
    } else if (typeof otherID === 'string' && isValid && otherID.length === 12) {
      otherID = this.id;
    } else if (otherID !== null && (otherID instanceof ObjectID || otherID.toHexString())) {
      return otherID.toHexString() === this.id.toHexString();
    } else {
      return false;
    }
  }

  /**
   * Generate a 12 byte id string used in ObjectID's.
   *
   * @method
   * @params {Number} [time] optional parameter allowing to pass in a second based timestamp.
   * @returns {Buffer} the 12 byte id binary string.
   */
  generate(time?: number): Buffer {
    if ('number' !== typeof time) time = ~~(Date.now() / 1000);

    // use pid
    const pid = (typeof process === 'undefined' ? Math.floor(Math.random() * 100000) : process.pid) % 0xFFFF;
    const inc = this.get_inc();
    // Buffer used
    const buffer = new Buffer(12);
    // Encode time
    buffer[3] = time & 0xff;
    buffer[2] = (time >> 8) & 0xff;
    buffer[1] = (time >> 16) & 0xff;
    buffer[0] = (time >> 24) & 0xff;
    // Encode machine
    buffer[6] = MACHINE_ID & 0xff;
    buffer[5] = (MACHINE_ID >> 8) & 0xff;
    buffer[4] = (MACHINE_ID >> 16) & 0xff;
    // Encode pid
    buffer[8] = pid & 0xff;
    buffer[7] = (pid >> 8) & 0xff;
    // Encode index
    buffer[11] = inc & 0xff;
    buffer[10] = (inc >> 8 & 0xff);
    buffer[9] = (inc >> 16 & 0xff);

    return buffer;
  }

  /**
   * Returns the generation date (accurate up to the second) that this ID was generated.
   *
   * @method
   * @returns {Date} generation date
   */
  getTimestamp(): Date {
    const timestamp = new Date();
    const time = this.id[3] | this.id[2] << 8 | this.id[1] << 16 | this.id[0] << 24;
    timestamp.setTime(Math.floor(time) * 1000);
    return timestamp;
  }

  /**
   * Return the ObjectID id as a 24 byte hex string representation.
   *
   * method
   * @returns {String} return the 24 byte hex string representation.
   */
  toHexString(): string {
    if (ObjectID.cacheHexString && this.__id) return this.__id;

    let hexString = '';
    if (!this.id || !this.id.length) {
      throw new Error('invalid ObjectId, ObjectId.id must be either a string or a Buffer, but is [' + JSON.stringify(this.id) + ']');
    }

    if (this.id instanceof Buffer) {
      hexString = convertToHex(this.id);
      if (ObjectID.cacheHexString) this.__id = hexString;
      return hexString;
    }

    for (let i = 0; i < this.id.length; i++) {
      hexString += hexTable[this.id.charCodeAt(i)];
    }

    if (ObjectID.cacheHexString) this.__id = hexString;
    return hexString;
  }

  /**
   * Converts the id into a 24 bytes hex string for printing
   *
   * @method
   * @param {String} [format] The Buffer toString format parameter.
   * @returns {String} return the 24 byte hex string representation.
   * @ignore
   */
  toString(format?: string): string {
    // Is the id a buffer then use the buffer toString method to return the format
    if (this.id && this.id.copy) {
      return this.id.toString(typeof format === 'string' ? format : 'hex');
    }

    return this.toHexString();
  }

  /**
   * Converts to a string representation of this id.
   *
   * @method
   * @returns {String} return the 24 byte hex string representation.
   * @ignore
   */
  inspect(): string {
    return this.toString();
  }

  /**
   * Converts to its JSON representation.
   *
   * @method
   * @returns {String} return the 24 byte hex string representation.
   * @ignore
   */
  toJSON(): string {
    return this.toHexString();
  }
}

// export ObjectID class
export { ObjectID };
export { ObjectID as ObjectId};