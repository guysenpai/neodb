/*!
 * NeoDB - Finder
 * Copyright (c) 2017 Guy Senpai <guy.senpai@gmail.com>
 * MIT Licensed
 */
"use strict";

/**
 * @typedef {Object} ObjectRef
 * @property {Object} $parent
 * @property {Object} $obj
 */
export interface ObjectRef {
  $parent: object;
  $obj: object;
}

/**
 * Finder
 * @class
 */
class Finder {

  /** @ignore */
  objects: ObjectRef[];

  /** @ignore */
  results: any[];

  /** @ignore */
  resultIDs: any;


  /**
   * @constructor
   */
  constructor() {
    this.objects = [];
    this.results = [];
    this.resultIDs = {};
  }


  /**
   * @method
   * @param {Array<Object>} objs
   * @param {Object} query
   * @param {Boolean} isMulti
   * @returns {Array.<Object>}
   */
  findAll(objs: any[], query: any, isMulti: boolean): any[] {
    for (let i = 0; i < objs.length; i++) {
      this._performSearch(objs[i], query, objs[i]);
      if (!isMulti && this.results.length === 1) return this.results;
    }

    while (this.objects.length !== 0) {
      const objRef = this.objects.pop();
      this._performSearch(objRef.$obj, query, objRef.$parent);
      if (!isMulti && this.results.length === 1) return this.results;
    }

    return this.results;
  }

  /**
   * @method
   * @param {Object} obj
   * @param {Object} query
   * @param {Object} [objParent]
   */
  private _performSearch(obj: any, query: any, objParent?: any): void {
    for (const criteria in query) {
      const newQuery = {};
      newQuery[criteria] = query[criteria];
      this._search(obj, newQuery, objParent);
    }

    for (let i = 0; i < this.results.length; i++) {
      const result = this.results[i];
      for (const field in query) {
        if (result[field] !== undefined && result[field] !== query[field]) {
          this.results.splice(i, 1);
        }
      }
    }
  }

  /**
   * @method
   * @param {Object} obj
   * @param {Object} query
   * @param {Object} [objParent]
   */
  private _search(obj: any, query: any, objParent?: any): void {
    for (const key in obj) {
      if (typeof obj[key] !== 'object') {
        if (query[key] === obj[key]) {
          if (objParent !== undefined && this.resultIDs[objParent._id] === undefined) {
            this.results.push(objParent);
            this.resultIDs[objParent._id] = objParent._id;
          } else if (this.resultIDs[obj._id] === undefined) {
            this.results.push(obj);
            this.resultIDs[obj._id] = obj._id;
          }
        }
      } else {
        let parent = obj;

        if (objParent !== undefined) parent = objParent;

        const objRef = {
          $obj: obj[key],
          $parent: parent,
        };
        this.objects.push(objRef);
      }
    }
  }
}

// export Finder class
export { Finder };