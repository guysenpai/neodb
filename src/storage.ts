/*!
 * NeoDB - Storage
 * Copyright (c) 2017 Guy Senpai <guy.senpai@gmail.com>
 * MIT Licensed
 */
"use strict";

import { existsSync, lstatSync, readdirSync, readFileSync, rmdirSync, unlinkSync, writeFileSync } from 'fs';
import merge = require('lodash.merge');
import { join } from 'path';
import mkdirp = require('mkdirp');

import { Collection } from './collection';

/**
 * Storage
 * @class
 */
class Storage {

  /** @const */
  static ENCODING = 'utf-8';

  /**
   * @static
   * @method
   * @param {String} file
   * @returns {Boolean}
   * @description Check if file exists
   */
  static isValidPath = existsSync;

  /**
   * @static
   * @method
   * @param {String} path
   * @returns {Array.<String>}
   * @description Read content from dir
   */
  static readFromPath = readdirSync;

  /**
   * @static
   * @method
   * @param {String} path
   * @returns {Object} fs.Stat object
   * @description Return Stat from path
   */
  static infoPath = lstatSync;

  /**
   * @static
   * @method
   * @param {String} file
   * @description Delete file
   */
  static removeFile = unlinkSync;

  /**
   * @static
   * @method
   * @param {String} path
   * @description Create folder and its parents if not exist
   */
  static newFolder = mkdirp.sync;

  /**
   * @static
   * @method
   * @param {String} path
   * @description Delete folder
   */
  static removeFolder = rmdirSync;

  /**
   * @static
   * @method
   * @param {String} path
   * @description Delete folder and all its content
   */
  static removeFolderRecursive(path: string): void {
    if (Storage.isValidPath(path)) {
      const files = Storage.readFromPath(path);

      for (let i = 0; i < files.length; i++) {
        const current = join(path, files[i]);
        if (Storage.infoPath(current).isDirectory()) {
          Storage.removeFolderRecursive(current);
        } else {
          Storage.removeFile(current);
        }
      }

      Storage.removeFolder(path);
    }
  }

  /**
   * @static
   * @method
   * @param {String} file
   * @param {Array.<Object>} content
   * @description Write content to file
   */
  static writeToFile = (file: string, content: object[] = []) => writeFileSync(file, JSON.stringify(content, null, '\t'));

  /**
   * @static
   * @method
   * @param {String} file
   * @returns {String}
   * @description Read content from file
   */
  static readFromFile = (file: string) => readFileSync(file, Storage.ENCODING);

  /**
   * @static
   * @method
   * @param {String} file
   * @param {Boolean} throwParseError
   * @return {Array.<Collection>}
   */
  static parseData(file: string, throwParseError?: boolean): Collection[] {
    try {
      return JSON.parse(String(Storage.readFromFile(file)));
    } catch (err) {
      if (throwParseError) throw err;
    }
    return [];
  }

  /**
   * @static
   * @method
   * @param {Array.<Collection>} collections
   * @param {Object} query
   * @param {Object} data
   * @param {Boolean} multi
   * @returns {Array.<Collection>}
   */
  static update(collections: Collection[], query: object, data: object, multi: boolean): Collection[] {
    // break 2 loops at once if multi is false
    loop:
    for (let i = (collections.length - 1); i >= 0; i--) {
      const collection = collections[i];

      for (const prop in query) {
        if (prop in collection && collection[prop] === query[prop]) {
          collections[i] = merge(collection, data);

          if (!multi) break loop;
        }
      }
    }

    return collections;
  }

  /**
   * @static
   * @method
   * @param {Array.<Collection>} collections
   * @param {Object} query
   * @param {Boolean} multi
   * @returns {Array.<Collection>}
   */
  static remove(collections: Collection[], query: object, multi: boolean): Collection[] {
    // break 2 loops at once if multi is false
    loop:
    for (let i = (collections.length - 1); i >= 0; i--) {
      const collection = collections[i];

      for (const prop in query) {
        if (prop in collection && collection[prop] === query[prop]) {
          collections.splice(i, 1);

          if (!multi) break loop;
        }
      }
    }

    return collections;
  }

  /**
   * @static
   * @method
   * @param {Array.<Collection>} collections
   * @param {Object} query
   * @param {Boolean} multi
   * @returns {Array.<Collection>}
   */
  static find(collections: Collection[], query: object, multi: boolean): Collection[] {
    const results = [];

    // break 2 loops at once if multi is false
    loop:
    for (let i = (collections.length - 1); i >= 0; i--) {
      const collection = collections[i];

      for (const prop in query) {
        if (prop in collection && collection[prop] === query[prop]) {
          results.push(collections[i]);

          if (!multi) break loop;
        }
      }
    }

    return results;
  }
}

// export Storage class
export { Storage };