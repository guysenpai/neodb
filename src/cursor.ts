/*!
 * NeODB - Cursor
 * Copyright (c) 2017 Guy Senpai <guy.senpai@gmail.com>
 * MIT Licensed
 */
"use strict";

import omit = require('lodash.omit');
import orderBy = require('lodash.orderby');
import pick = require('lodash.pick');
import { join } from 'path';

import { Collection } from './collection';
import { DB } from './db';
import { Finder } from './finder';
import { Storage } from './storage';

/**
 * Cursor
 * @class
 * @property {String} sortValue
 */
class Cursor {

  /** @type {String} */
  sortValue: string;

  /** @ignore */
  _db: DB;

  /** @ignore */
  private _query: any;

  /** @ignore */
  private _collections: Collection[];

  /** @ignore */
  private _fields: any;

  /** @ignore */
  private _limit: number;

  /** @ignore */
  private _skip: number;

  /** @ignore */
  private _sort: any|any[];


  /**
   * @constructor
   * @param {DB} db
   * @param {Object} query
   * @param {Array.<Collection>} collections Collections buffer
   */
  constructor(db: DB, query: any, collections: Collection[]) {
    this._db = db;
    this._query = query || {};
    this._collections = collections;
  }


  /**
   * @method
   * @param {Boolean} applySkipLimit
   * @param {Object} [options]
   * @param {Number} [options.limit]
   * @param {Number} [options.skip]
   * @param {Cursor~CountResultCallback} callback
   * @returns {Promise}
   */
  count(applySkipLimit: boolean, options: any, callback: CountResultCallback);
  count(applySkipLimit: boolean, callback: CountResultCallback);
  count(callback: CountResultCallback);

  count(applySkipLimit?: boolean, options?: any): Promise<number>;

  count(applySkipLimit?: any, options?: any, callback?: CountResultCallback): Promise<number> | void {
    if (this._query === null) throw new Error("count can only be used with find command");
    if (typeof applySkipLimit === 'function') {
      callback = applySkipLimit;
      applySkipLimit = true;
    }
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};

    if (applySkipLimit) {
      if (typeof this._skip === 'number') options.skip = this._skip;
      if (typeof this._limit === 'number') options.limit = this._limit;
    }

    // Find filtered collections
    const candidates = (new Finder()).findAll(this._collections, this._query, true);
    const results = candidates.slice(options.skip, (options.skip + options.limit));

    // Execute with callback
    if (typeof callback === 'function') return callback(null, results.length);

    // Return promise
    return new Promise((resolve, reject) => resolve(results.length));
  }

  /**
   * @method
   * @param {Object} filter
   * @returns {Cursor}
   */
  filter(filter: any): this {
    this._query = filter;
    return this;
  }

  /**
   * @method
   * @param {Number} value
   * @returns {Cursor}
   */
  limit(value: number): this {
    if (typeof value !== 'number') throw new Error("limit requires an integer");
    this._limit = value;
    return this;
  }

  /**
   * @method
   * @param {Object} value
   * @returns {Cursor}
   */
  project(value: any): this {
    this._fields = value;
    return this;
  }

  /**
   * @method
   * @param {Array.<Collection>} candidates
   * @returns {Array.<Collection>}
   */
  projection(candidates: Collection[]): Collection[] {
    if (!this._fields || Object.keys(this._fields).length === 0) {
      return candidates;
    }

    const keepId = this._fields._id === 0 ? true : false;
    this._fields = omit(this._fields, '_id');

    // Check for consistency
    let action;
    const keys = Object.keys(this._fields);
    keys.forEach((k) => {
      if (action && this._fields[k] !== action) throw new Error("can't both keep and omit fields except for _id");
      action = this._fields[k];
    });

    // Do the projection
    const results = [];
    for (let i = 0; i < candidates.length; i++) {
      let candidate = candidates[i];
      if (action === 1) {
        // pick-type projection
        candidate = pick(candidate, keys);
      } else {
        // omit-type projection
        candidate = omit(candidate, keys);
      }

      // Apply keep id
      if (!keepId) delete candidate._id;

      results.push(candidate);
    }

    return results;
  }

  /**
   * @method
   * @param {Number} value
   * @returns {Cursor}
   */
  skip(value: number): this {
    if (typeof value !== 'number') throw new Error("skip requires an integer");
    this._skip = value;
    return this;
  }

  /**
   * @method
   * @param {String|Array|Object} keyOrList
   * @param {Number} [direction]
   * @returns {Cursor}
   */
  sort(keyOrList: any, direction?: number): this {
    let order = keyOrList;

    // Format if `order` is in the form `{field1: '(1|-1)'}`
    if (typeof order === 'object' && !Array.isArray(order)) {
      const orderList = [];
      for (const k in order) {
        orderList.push([k, order[k]]);
      }
      order = orderList;
    } else if (Array.isArray(order) && !Array.isArray(order[0])) {
      // Format if `order` is in the form `[{field1: '(1|-1)'},...]`
      const orderList = [];
      for (const obj of order) {
        for (const k in obj) {
          orderList.push([k, obj[k]]);
        }
      }
      order = orderList;
    }

    if (typeof direction === 'number') {
      order = [[keyOrList, direction]];
    }

    // Format if `order` is in the form `[['field1': '(1|-1)'], [...]]`
    if (Array.isArray(order) && Array.isArray(order[0])) {
      order = order.map((x) => {
        const value = [x[0], null];
        if (x[1] === 1) value[1] = 'asc';
        else if (x[1] === -1) value[1] = 'desc';
        else if (x[1] === 'asc' || x[1] === 'desc') value[1] = x[1];
        else throw new Error("Illegal sort clause, must be the form [['field1', '(1|-1)'], ['field2', '(1|-1)']]");

        return value;
      });
    }

    this._sort = order;
    this.sortValue = order;
    return this;
  }

  /**
   * @method
   * @param {Cursor~ToArrayResultCallback} callback
   * @returns {Promise}
   */
  toArray(callback: ToArrayResultCallback);

  toArray(): Promise<Collection[]>;

  toArray(callback?: ToArrayResultCallback): Promise<Collection[]> | void {
    // Find filtered collections
    let candidates = (new Finder()).findAll(this._collections, this._query, true);

    if (Object.keys(this._query).length === 0) candidates = this._collections;

    let docs = [];
    // Apply skip and limit
    const skip = this._skip || 0;
    const limit = this._limit || candidates.length;

    if (!this._sort) {
      docs = candidates.slice(skip, (skip + limit));
    }

    // Apply all sorts
    if (this._sort) {
      // Sorting
      const fields = [];
      const orders = [];

      for (const [field, order] of this._sort) {
        fields.push(field);
        orders.push(order);
      }

      docs = orderBy(candidates, fields, orders);
      docs = docs.slice(skip, (skip + limit));
    }

    // Apply projection
    try {
      docs = this.projection(docs);
    } catch (err) {
      if (callback) return callback(err);
      return new Promise((resolve, reject) => reject(err));
    }

    // Execute with callback
    if (typeof callback === 'function') return callback(null, docs);

    // Return promise
    return new Promise((resolve, reject) => resolve(docs));
  }
}

// export Cursor class
export { Cursor };

/**
 * @callback Cursor~CountResultatCallback
 * @param {Error} error
 * @param {Number} result
 */
export type CountResultCallback = (error: Error, result?: number) => void;

/**
 * @callback Cursor~ToArrayResultCallback
 * @param {Error} error
 * @param {Array.<Collection>} docs
 */
export type ToArrayResultCallback = (error: Error, docs?: Collection[]) => void;