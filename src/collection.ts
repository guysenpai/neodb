/*!
 * NeoDB - Collection
 * Copyright (c) 2017 Guy Senpai <guy.senpai@gmail.com>
 * MIT Licensed
 */
"use strict";

import merge = require('lodash.merge');
import { join } from 'path';

import { Cursor } from './cursor';
import { DB } from './db';
import { Finder } from './finder';
import { ObjectID } from './objectid';
import { Storage } from './storage';

/**
 * Collection
 * @class
 * @property {String} collectionName
 */
class Collection {

  /** @ignore */
  db: DB;

  /** @ignore */
  collectionName: string;

  /** @ignore */
  options: any;

  /** @ignore */
  private _file: string;

  /** @ignore */
  [property: string]: any;


  /**
   * @constructor
   * @param {DB} db
   * @param {String} name
   * @param {Object} [options]
   */
  constructor(db: DB, name: string, options?: any) {
    this.db = db;
    this.collectionName = name;
    this.options = options || {};
    this._file = join(this.db.databasePath, `${name}.json`);
  }


  /**
   * @method
   * @param {Object} query
   * @param {Collection~ResultCallback} [callback]
   * @returns {Cursor}
   */
  find(query: any, callback: ResultCallback);

  find(query: any): Cursor;

  find(query: any, callback?: ResultCallback): Cursor | void {
    const collections = Storage.parseData(this._file);

    const cursor = new Cursor(this.db, query, collections);

    if (typeof callback === 'function') callback(null, cursor);

    return cursor;
  }

  /**
   * @method
   * @param {Object} query
   * @param {Object} [options]
   * @param {Object} [options.fields]
   * @param {Number} [options.limit]
   * @param {Number} [options.skip]
   * @param {Array|Object} [options.sort]
   * @param {Collection~ResultCallback} [callback]
   * @returns {Promise}
   */
  findOne(query: any, options: any, callback: ResultCallback);
  findOne(query: any, callback: ResultCallback);

  findOne(query: any, options?: any): Promise<Collection>;

  findOne(query: any, options?: any, callback?: ResultCallback): Promise<Collection> | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};
    options = merge({}, this.options, options);

    const cursor = this.find(query);

    if (options.fields) cursor.project(options.fields);
    if (options.limit) cursor.limit(options.limit);
    if (options.skip) cursor.skip(options.skip);
    if (options.sort) cursor.sort(options.sort);

    if (typeof callback === 'function') return cursor.toArray((err, docs) => callback(err, docs[0]));

    return new Promise((resolve, reject) => {
      cursor.toArray((err, docs) => {
        if (err) return reject(err);
        return resolve(docs[0]);
      });
    });
  }

  /**
   * @method
   * @param {Array.<Object>} docs
   * @param {Object} [options]
   * @param {Boolean} [options.ordered]
   * @param {Collection~InsertOpCallback} [callback]
   * @returns {Promise}
   */
  insertMany(docs: any[], options: any, callback: InsertOpCallback);
  insertMany(docs: any[], callback: InsertOpCallback);

  insertMany(docs: any[], options?: any): Promise<InsertOpResult>;

  insertMany(docs: any[], options?: any, callback?: InsertOpCallback): Promise<InsertOpResult> | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || { ordered: true };

    if (!Array.isArray(docs)) {
      if (typeof callback === 'function') return callback(new Error("docs parameter must be an array of documents"));

      return new Promise((resolve, reject) => reject(new Error("docs parameter must be an array of documents")));
    }

    // Get parsed collection buffer
    const collections = Storage.parseData(this._file);
    // the insert operation result
    const result = {
      connection: this.db,
      insertedCount: 0,
      insertedIds: [],
      ops: [],
    };

    for (let i = 0; i < docs.length; i++) {
      // Add _id if not specified
      if (!docs[i]._id) docs[i]._id = ObjectID.createPk();
      // Add to collection buffer
      collections.push(docs[i]);
      // Add inserted id to result
      result.insertedIds.push(docs[i]._id);
    }

    // Save to data file
    Storage.writeToFile(this._file, collections);

    // Set result inserted count
    result.insertedCount = docs.length;
    // Set result ops
    result.ops = docs;

    // Execute using callback
    if (typeof callback === 'function') return callback(null, result);

    // return a promise
    return new Promise((resolve, reject) => resolve(result));
  }

  /**
   * @method
   * @param {Object} doc
   * @param {Collection~InsertOneOpCallback} [callback]
   * @returns {Promise}
   */
  insertOne(doc: any, callback: InsertOneOpCallback);

  insertOne(doc: any): Promise<InsertOneOpResult>;

  insertOne(doc: any, callback?: InsertOneOpCallback): Promise<InsertOneOpResult> | void {
    if (Array.isArray(doc) && typeof callback === 'function') {
      return callback(new Error("doc parameter must be an object"));
    } else if (Array.isArray(doc)) {
      return new Promise((resolve, reject) => {
        reject(new Error("doc parameter must be an object"));
      });
    }

    // Get parsed collection buffer
    const collections = Storage.parseData(this._file);
    // the insert operation result
    const result = {
      connection: this.db,
      insertedCount: 0,
      insertedId: null,
      ops: [],
    };

    // Add _id if not specified
    if (!doc._id) doc._id = ObjectID.createPk();
    // Add to collection buffer
    collections.push(doc);
    // Add inserted id to result
    result.insertedId = doc._id;

    // Save to data file
    Storage.writeToFile(this._file, collections);

    // Set result inserted count
    result.insertedCount = 1;
    // Set result ops
    result.ops = [doc];

    // Execute using callback
    if (typeof callback === 'function') return callback(null, result);

    // return a promise
    return new Promise((resolve, reject) => resolve(result));
  }

  /**
   * @method
   * @param {Object} query
   * @param {Object} update
   * @param {Object} [options]
   * @param {Boolean} [options.upsert]
   * @param {Collection~UpdateOpCallback} [callback]
   * @returns {Promise}
   */
  updateMany(query: any, update: any, options: any, callback: UpdateOpCallback);
  updateMany(query: any, update: any, callback: UpdateOpCallback);

  updateMany(query: any, update: any, options?: any): Promise<UpdateOpResult>;

  updateMany(query: any, update: any, options?: any, callback?: UpdateOpCallback): Promise<UpdateOpResult> | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};
    options.multi = true;

    // Get parsed collection buffer
    let collections = Storage.parseData(this._file);

    // find records which matches query
    const records = Storage.find(collections, query, options.multi);

    // the update/upsert operation result
    const result = {
      connection: this.db,
      matchedCount: 0,
      modifiedCount: 0,
      upsertedCount: 0,
      upsertedId: { _id: null },
    };

    if (Object.keys(update)[0] === '$set') update = update.$set;

    if (records.length) {
      // update to collection buffer
      collections = Storage.update(collections, query, update, options.multi);
      // Set result info
      result.matchedCount = records.length;
      result.modifiedCount = records.length;
    }

    if (!records.length && options.upsert) {
      if (!update._id) update._id = ObjectID.createPk();
      collections.push(update);
      result.upsertedCount = 1;
      result.upsertedId._id = update._id;
    }

    // Save to data file
    Storage.writeToFile(this._file, collections);

    // Execute using callback
    if (typeof callback === 'function') return callback(null, result);

    // return a promise
    return new Promise((resolve, reject) => resolve(result));
  }

  /**
   * @method
   * @param {Object} query
   * @param {Object} update
   * @param {Object} [options]
   * @param {Boolean} [options.upsert]
   * @param {Collection~UpdateOpCallback} [callback]
   * @returns {Promise}
   */
  updateOne(query: any, update: any, options: any, callback: UpdateOpCallback);
  updateOne(query: any, update: any, callback: UpdateOpCallback);

  updateOne(query: any, update: any, options?: any): Promise<UpdateOpResult>;

  updateOne(query: any, update: any, options?: any, callback?: UpdateOpCallback): Promise<UpdateOpResult> | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};
    options.multi = false;

    // Get parsed collection buffer
    let collections = Storage.parseData(this._file);

    // find records which matches query
    const records = Storage.find(collections, query, options.multi);

    // the update/upsert operation result
    const result = {
      connection: this.db,
      matchedCount: 0,
      modifiedCount: 0,
      upsertedCount: 0,
      upsertedId: { _id: null },
    };

    if (Object.keys(update)[0] === '$set') update = update.$set;

    if (records.length) {
      // update to collection buffer
      collections = Storage.update(collections, query, update, options.multi);
      // Set result info
      result.matchedCount = records.length;
      result.modifiedCount = records.length;
    }

    if (!records.length && options.upsert) {
      if (!update._id) update._id = ObjectID.createPk();
      collections.push(update);
      result.upsertedCount = 1;
      result.upsertedId._id = update._id;
    }

    // Save to data file
    Storage.writeToFile(this._file, collections);

    // Execute using callback
    if (typeof callback === 'function') return callback(null, result);

    // return a promise
    return new Promise((resolve, reject) => resolve(result));
  }

  /**
   * @method
   * @param {Object} query
   * @param {Collection~DeleteOpCallback} [callback]
   * @returns {Promise}
   */
  deleteMany(query: any, callback: DeleteOpCallback);

  deleteMany(query: any): Promise<DeleteOpResult>;

  deleteMany(query: any, callback?: DeleteOpCallback): Promise<DeleteOpResult> | void {
    // Get parsed collection buffer
    let collections = Storage.parseData(this._file);

    // find records which matches query
    const records = Storage.find(collections, query, true);

    // the delete operation result
    const result = {
      connection: this.db,
      deletedCount: 0,
    };

    if (records.length) {
      // remove to collection buffer
      collections = Storage.remove(collections, query, true);
      // Set result info
      result.deletedCount = records.length;
    }

    if (!query || Object.keys(query).length === 0) {
      collections = [];
      result.deletedCount = collections.length;
    }

    // Save to data file
    Storage.writeToFile(this._file, collections);

    // Execute using callback
    if (typeof callback === 'function') return callback(null, result);

    // return a promise
    return new Promise((resolve, reject) => resolve(result));
  }

  /**
   * @method
   * @param {Object} query
   * @param {Collection~DeleteOpCallback} [callback]
   * @returns {Promise}
   */
  deleteOne(query: any, callback: DeleteOpCallback);

  deleteOne(query: any): Promise<DeleteOpResult>;

  deleteOne(query: any, callback?: DeleteOpCallback): Promise<DeleteOpResult> | void {
    // Get parsed collection buffer
    let collections = Storage.parseData(this._file);

    // find records which matches query
    const records = Storage.find(collections, query, false);

    // the delete operation result
    const result = {
      connection: this.db,
      deletedCount: 0,
    };

    if (records.length) {
      // remove to collection buffer
      collections = Storage.remove(collections, query, false);
      // Set result info
      result.deletedCount = records.length;
    }

    // Save to data file
    Storage.writeToFile(this._file, collections);

    // Execute using callback
    if (typeof callback === 'function') return callback(null, result);

    // return a promise
    return new Promise((resolve, reject) => resolve(result));
  }

  /**
   * @method
   * @param {Object} query
   * @param {Object} [options]
   * @param {Number} [options.limit]
   * @param {Number} [options.skip]
   * @param {Collection~CountCallback} [callback]
   * @returns {Promise}
   */
  count(query: any, options: any, callback: CountCallback);
  count(query: any, callback: CountCallback);

  count(query: any, options?: any): Promise<number>;

  count(query: any, options?: any, callback?: CountCallback): Promise<number> | void {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};

    // Get parsed collection buffer
    const collections = Storage.parseData(this._file);

    // find records which matches query
    const records = Storage.find(collections, query, options.multi);

    // the count operation result
    let result = 0;
    if (query && Object.keys(query).length && records.length) {
      result = records.length;
    } else if (!query || Object.keys(query).length === 0) {
      result = collections.length;
    }

    // Execute using callback
    if (typeof callback === 'function') return callback(null, result);

    // return a promise
    return new Promise((resolve, reject) => resolve(result));
  }
}

// export collection class
export { Collection };

/**
 * @callback Collection~ResultCallback
 * @param {Error} error
 * @param {Object|Array.<Object>} result
 */
export type ResultCallback = (error: Error, result?: any|any[]) => void;

/**
 * @callback Collection~InsertOneOpCallback
 * @param {Error} error
 * @param {Collection~InsertOneOpResult} result
 */
export type InsertOneOpCallback = (error: Error, result?: InsertOneOpResult) => void;

/**
 * @typedef {Object} Collection~InsertOneOpResult
 * @property {DB} connection
 * @property {Number} insertedCount
 * @property {ObjectID} insertedId
 * @property {Array.<Object>} ops
 */
export interface InsertOneOpResult {
  connection: DB;
  insertedCount: number;
  insertedId: ObjectID;
  ops: any[];
}

/**
 * @callback Collection~InsertOpCallback
 * @param {Error} error
 * @param {Collection~InsertOpResult} result
 */
export type InsertOpCallback = (error: Error, result?: InsertOpResult) => void;

/**
 * @typedef {Object} Collection~InsertOpResult
 * @property {DB} connection
 * @property {Number} insertedCount
 * @property {Array.<ObjectID>} insertedIds
 * @property {Array.<Object>} ops
 */
export interface InsertOpResult {
  connection: DB;
  insertedCount: number;
  insertedIds: ObjectID[];
  ops: any[];
}

/**
 * @callback Collection~UpdateOpCallback
 * @param {Error} error
 * @param {Collection~UpdateOpResult} result
 */
export type UpdateOpCallback = (error: Error, result?: UpdateOpResult) => void;

/**
 * @typedef {Object} Collection~UpdateOpResult
 * @property {DB} connection
 * @property {Number} matchedCount
 * @property {Number} modifiedCount
 * @property {Number} upsertedCount
 * @property {ObjectID} upsertedId
 */
export interface UpdateOpResult {
  connection: DB;
  matchedCount: number;
  modifiedCount: number;
  upsertedCount: number;
  upsertedId: { _id: ObjectID };
}

/**
 * @callback Collection~DeleteOpCallback
 * @param {Error} error
 * @param {Collection~DeleteOpResult} result
 */
export type DeleteOpCallback = (error: Error, result?: DeleteOpResult) => void;

/**
 * @typedef {Object} Collection~DeleteOpResult
 * @property {DB} connection
 * @property {Number} deletedCount
 */
export interface DeleteOpResult {
  connection: DB;
  deletedCount: number;
}

/**
 * @callback Collection~CountCallback
 * @param {Error} error
 * @param {Number} result
 */
export type CountCallback = (error: Error, result?: number) => void;