# NeODB

NeODB is a node embedded object database base on JSON and written in TypeScript with a MongoDB like API 2.x.

## Contents

- [Getting started](#getting-started)
- [Documentation](#documentation)
    * [Connect to database](#connect-to-neodb)
    * [Insert documents](#insert-documents)
    * [Updating documents](#updating-documents)
    * [Removing documents](#removing-documents)
    * [Finding documents](#finding-documents)

## Getting started

Install the module locally:

```bash
$ npm install neodb
```

As stated, the API is fully compatible with MongoDB. The only differences are the client and the connection URL. Consider this MongoDB code:

```javascript
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/myproject';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log('Connected successfully to server');
  
  // Get the documents collection
  var collection = db.collection('documents');
  
  // Insert some documents
  collection.insertMany([{a: 1}, {a: 2}, {a: 3}], function(err, result) {
    assert.equal(null, err);
    assert.equal(3, result.ops.length);
    console.log('Inserted 3 documents into the collection');
    
    // Find some documents
    collection.find({}).toArray(function(err, docs) {
      assert.equal(null, err);
      console.log('Found the following records');
      console.log(docs);
      db.close();
    });
  });
});
```

The same example using NeODB is as follow:

```javascript
var DB = require('mongodb').DB
  , assert = require('assert');

// Connection URL
var url = 'myproject';

// Use connect method to connect to the database
DB.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log('Connected successfully to database');
  
  // Get the documents collection
  var collection = db.collection('documents');
  
  // Insert some documents
  collection.insertMany([{a: 1}, {a: 2}, {a: 3}], function(err, result) {
    assert.equal(null, err);
    assert.equal(3, result.ops.length);
    console.log('Inserted 3 documents into the collection');
    
    // Find some documents
    collection.find({}).toArray(function(err, docs) {
      assert.equal(null, err);
      console.log('Found the following records');
      console.log(docs);
    });
  });
});
```

## Documentation

### Connect to NeODB

To connect to a single `NeODB` database, specify the name of the `NeODB` database to connect to.

```javascript
var DB = require('mongodb').DB
  , assert = require('assert');

// Connection URL
var url = 'myproject';

// Use connect method to connect to the database
DB.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log('Connected successfully to database');
});
```

Note the connection `url` could be the name of database folder or the path of the database folder.

### Insert Documents

The `insertMany` and `insertOne` methods exist on the `Collection` class and are used to insert documents into NeODB.

```javascript
// Get the documents collection
var collection = db.collection('documents');
  
// Insert multiple documents
collection.insertMany([{a: 1}, {a: 2}, {a: 2}], function(err, result) {
  assert.equal(null, err);
  assert.equal(3, result.ops.length);
  console.log('Inserted 3 documents into the collection');
});

// Insert a single document
collection.insertOne({a: 3}, function(err, result) {
  assert.equal(null, err);
  assert.equal(1, result.ops.length);
  console.log('Inserted 1 document into the collection');
});
```

### Updating Documents

The `updateMany` and `updateOne` methods exist on the `Collection` class and are used to update and upsert documents.

```javascript
// Get the documents collection
var collection = db.collection('documents');
  
// Update a single document
collection.updateOne({a: 1}, {$set: {b: 1}}, function(err, result) {
  assert.equal(null, err);
  assert.equal(1, result.matchedCount);
  assert.equal(1, result.modifiedCount);
});

// Update multiple documents
collection.updateMany({a: 2}, {$set: {b: 1}}, function(err, result) {
  assert.equal(null, err);
  assert.equal(2, result.matchedCount);
  assert.equal(2, result.modifiedCount);
});

// Upsert a single document
collection.updateOne({a: 4}, {$set: {b: 1}}, {upsert: true}, function(err, result) {
  assert.equal(null, err);
  assert.equal(0, result.matchedCount);
  assert.equal(1, result.upsertedCount);
});
```

The `update` method also accepts a third argument which can be an options object. This object can have the following fields:

Parameter | Type                      | Description
----------|---------------------------|------------------------------
upsert    | Boolean (default: false)  | Update operation is an upsert

### Removing Documents

The `deleteMany` and `deleteOne` methods exist on the `Collection` class and are used to remove documents from NeODB.

```javascript
// Get the documents collection
var collection = db.collection('documents');
  
// Remove a single document
collection.deleteOne({a: 1}, function(err, result) {
  assert.equal(null, err);
  assert.equal(1, result.deletedCount);
});

// Remove multiple documents
collection.deleteMany({a: 2}, function(err, result) {
  assert.equal(null, err);
  assert.equal(2, result.deletedCount);
});
```

### Finding Documents

The main method for querying the database is the `find` method. `find` return an instance of `Cursor` class which allows to operate on the data.

```javascript
// Get the documents collection
var collection = db.collection('documents');
  
// Insert multiple documents
collection.insertMany([{a: 1}, {a: 2}, {a: 2}], function(err, result) {
  assert.equal(null, err);
  assert.equal(3, result.insertedCount);
    
  // Find all documents that match query
  collection.find({a: 2}).toArray(function(err, docs) {
    assert.equal(null, err);
    assert.equal(2, docs.length);
  });
});
```

The _cursor_ returned by the `find` method has several method that allow for chaining of options for the query. Once the query is ready to be executed you can retrieve the documents using `toArray` method. The `toArray` method materializes all the documents.

```javascript
collection.find({}).project({a:1})                    // create a projection of field `a`
collection.find({}).skip(1).limit(10)                 // Skip 1 limit 10
collection.find({}).filter({a:1})                     // Set query on the cursor
collection.find({}).sort([['a', 1]])                  // Set the sort order of the cursor query
```

All options are chainable, so you can combine settings in the following way:

```javascript
collection.find({}).project({a:1}).filter({a:1}).skip(1).limit(10).sort([['a', 1]]).toArray(...)
```

## Todo

- [ ] Implement `replaceOne` method to the `Collection` class
- [ ] Implement `findOneAndUpdate`, `findOneAndDelete` and `findOneAndReplace` methods to the `Collection` class
- [ ] Implement `bulkWrite` method to the `Collection` class which perform bulk operations
- [ ] Implement `next` and `each` method to the `Cursor` class

# License

MIT License 

Copyright (c) 2017 Guy Senpai 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
