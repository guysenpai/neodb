"use strict";

import * as chai from 'chai';

import { Collection } from '../src/collection';
import { DB } from '../src/index';
import { ObjectID } from '../src/objectid';

chai.should();

describe('Database', () => {
  
  describe('#connect()', () => {
    describe('new database', () => {
      afterEach((done) => {
        DB.connect('test', (err, db) => {
          if (!err) db.dropDatabase((err, droped) => done());
        });
      });
      
      context('when options.autocreate is false', () => {
        it('should return an error in callback', () => {
          DB.connect('test', { autocreate: false }, (err, db) => {
            err.should.be.an.instanceof(Error);
          });
        });
        
        it('should return an error in promise', () => {
          DB.connect('test', { autocreate: false }).catch(err => {
            err.should.be.an.instanceof(Error);
          });
        });
      });

      context('when options.autocreate is default true', () => {
        it('should return new created database in callback', () => {
          DB.connect('test', (err, db) => {
            db.should.be.an.instanceof(DB);
            db.databaseName.should.equal('test');
            db.databasePath.should.equal('.data/test');
          });
        });
        
        it('should return new created database in promise', () => {
          DB.connect('test').then(db => {
            db.should.be.an.instanceof(DB);
            db.databaseName.should.equal('test');
            db.databasePath.should.equal('.data/test');
          });
        });
      });
    });
  
    describe('existing database', () => {
      beforeEach((done) => {
        DB.connect('test', (err, db) => done());
      });
      
      afterEach((done) => {
        DB.connect('test', (err, db) => {
          if (!err) db.dropDatabase((err, droped) => done());
        });
      });
      
      it('should return existing database in callback', () => {
        DB.connect('test', (err, db) => {
          db.should.be.an.instanceof(DB);
          db.databaseName.should.equal('test');
          db.databasePath.should.equal('.data/test');
        });
      });
      
      it('should return existing database in promise', () => {
        DB.connect('test').then(db => {
          db.should.be.an.instanceof(DB);
          db.databaseName.should.equal('test');
          db.databasePath.should.equal('.data/test');
        });
      });
    });
  });
  
  describe('#createCollection()', () => {
    context('when collection does not exist', () => {
      beforeEach((done) => {
        DB.connect('test', (err, db) => done());
      });
      
      afterEach((done) => {
        DB.connect('test', (err, db) => {
          if (!err) db.dropDatabase((err, droped) => done());
        });
      });
      
      it('should return new created collection in callback (strict mode)', () => {
        DB.connect('test', (err, db) => {
          db.createCollection('users', { strict: true }, (err, collection) => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
      
      it('should return new created collection in callback', () => {
        DB.connect('test', (err, db) => {
          db.createCollection('users', (err, collection) => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
      
      it('should return new created collection in promise (strict mode)', () => {
        DB.connect('test').then(db => {
          db.createCollection('users', { strict: true }).then(collection => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
      
      it('should return new created collection in promise', () => {
        DB.connect('test').then(db => {
          db.createCollection('users').then(collection => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
    });
    
    context('when collection already exists', () => {
      beforeEach((done) => {
        DB.connect('test', (err, db) => {
          db.createCollection('users', (err, coll) => done());
        });
      });
      
      afterEach((done) => {
        DB.connect('test', (err, db) => {
          db.dropDatabase((err, droped) => done());
        });
      });
      
      it('should return an error in callback (strict mode)', () => {
        DB.connect('test', (err, db) => {
          db.createCollection('users', { strict: true }, (err, collection) => {
            err.should.exist;
            err.should.be.an.instanceof(Error);
          });
        });
      });
      
      it('should return collection in callback', () => {
        DB.connect('test', (err, db) => {
          db.createCollection('users', (err, collection) => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
      
      it('should return an error in promise (strict mode)', () => {
        DB.connect('test').then(db => {
          db.createCollection('users', { strict: true }).catch(err => {
            err.should.exist;
            err.should.be.an.instanceof(Error);
          });
        });
      });
      
      it('should return collection in promise', () => {
        DB.connect('test').then(db => {
          db.createCollection('users').then(collection => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
    });
  });
  
  describe('#collection()', () => {
    context('when collection does not exist', () => {
      beforeEach((done) => {
        DB.connect('test', (err, db) => done());
      });
      
      afterEach((done) => {
        DB.connect('test', (err, db) => {
          db.dropDatabase((err, droped) => done());
        });
      });
      
      it('should return an error in callback (strict mode)', () => {
        DB.connect('test', (err, db) => {
          db.collection('users', { strict: true }, (err, collection) => {
            err.should.exist;
            err.should.be.an.instanceof(Error);
          });
        });
      });
      
      it('should return new created collection in callback', () => {
        DB.connect('test', (err, db) => {
          db.collection('users', (err, collection) => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
      
      it('should throw an error (strict mode)', () => {
        DB.connect('test', (err, db) => {
          try {
            db.collection('users', { strict: true });
          } catch (err) {
            err.should.exist;
            err.should.be.an.instanceof(Error);
          }
        });
      });
      
      it('should return new created collection', () => {
        DB.connect('test', (err, db) => {
          try {
            const collection = db.collection('users');
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          } catch (err) { }
        });
      });
    });
    
    context('when collection already exists', () => {
      beforeEach((done) => {
        DB.connect('test', (err, db) => {
          db.createCollection('users', (err, coll) => done());
        });
      });
      
      afterEach((done) => {
        DB.connect('test', (err, db) => {
          db.dropDatabase((err, droped) => done());
        });
      });
      
      it('should return collection in callback (strict mode)', () => {
        DB.connect('test', (err, db) => {
          db.collection('users', { strict: true }, (err, collection) => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
      it('should return collection in callback', () => {
        DB.connect('test', (err, db) => {
          db.collection('users', (err, collection) => {
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          });
        });
      });
      
      it('should return collection (strict mode)', () => {
        DB.connect('test', (err, db) => {
          try {
            const collection = db.collection('users', { strict: true });
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          } catch (err) { }
        });
      });
      
      it('should return collection', () => {
        DB.connect('test', (err, db) => {
          try {
            const collection = db.collection('users');
            collection.should.exist;
            collection.should.be.an.instanceof(Collection);
          } catch (err) { }
        });
      });
    });
  });
  
  describe('#collections()', () => {});
  
  
  // Operations
  describe('Insert documents', () => {
    beforeEach((done) => {
      DB.connect('test', (err, db) => {
        db.createCollection('users', (err, coll) => done());
      });
    });
      
    afterEach((done) => {
      DB.connect('test', (err, db) => {
        db.dropDatabase((err, droped) => done());
      });
    });
    
    it('should insert many documents and return result (callback)', () => {
      const user1 = { username: 'user1', password: 'password1' };
      const user2 = { username: 'user2', password: 'password2' };
      
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.insertMany([user1, user2], (err, res) => {
            res.connection.databaseName.should.equal('test');
            res.insertedCount.should.equal(2);
            res.insertedIds.length.should.equal(2);
            res.ops.length.should.equal(2);
          });
        });
      });
    });
    
    it('should insert one document and return result (callback)', () => {
      const user = { username: 'user', password: 'password' };
      
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.insertOne(user, (err, res) => {
            res.connection.databaseName.should.equal('test');
            res.insertedCount.should.equal(1);
            res.insertedId.should.be.an.instanceof(ObjectID);
            res.ops.length.should.equal(1);
          });
        });
      });
    });
    
    it('should insert many documents and return result (promise)', () => {
      const user3 = { username: 'user3', password: 'password3' };
      const user4 = { username: 'user4', password: 'password4' };
      
      DB.connect('test').then(db => {
        db.collection('users', (err, users) => {
          users.insertMany([user3, user4]).then(res => {
            res.connection.databaseName.should.equal('test');
            res.insertedCount.should.equal(2);
            res.insertedIds.length.should.equal(2);
            res.ops.length.should.equal(2);
          });
        });
      });
    });
    
    it('should insert one document and return result (promise)', () => {
      const user5 = { username: 'user5', password: 'password5' };
      
      DB.connect('test').then(db => {
        db.collection('users', (err, users) => {
          users.insertOne(user5).then(res => {
            res.connection.databaseName.should.equal('test');
            res.insertedCount.should.equal(1);
            res.insertedId.should.be.an.instanceof(ObjectID);
            res.ops.length.should.equal(1);
          });
        });
      });
    });
  });
  
  describe('Find documents', () => {
    before((done) => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          const user1 = { name: 'user1', age: 25 };
          const user2 = { name: 'user2', age: 22 };
          const user3 = { name: 'user3', age: 29 };
          const user4 = { name: 'user4', age: 25 };
          
          users.insertMany([user1, user2, user3, user4], (err, res) => done());
        });
      });
    });
    
    after((done) => {
      DB.connect('test', (err, db) => {
        db.dropDatabase((err, droped) => done());
      });
    });
    
    it('should find all documents and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.find({}, (err, cursor) => {
            cursor.toArray((err, res) => {
              res.length.should.equal(4);
            });
          });
        });
      });
    });
    
    it('should find many documents by query and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.find({age: 25}, (err, cursor) => {
            cursor.toArray((err, res) => {
              res.length.should.equal(2);
            })
          });
        });
      });
    });
    
    it('should find one document and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.findOne({}, (err, user) => {
            user.name.should.equal('user1');
            user.age.should.equal(25);
          });
        });
      });
    });
    
    it('should find one document by query and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.findOne({age: 29}, (err, user) => {
            user.name.should.equal('user3');
            user.age.should.equal(29);
          });
        });
      });
    });
    
    it('should find all documents and return result (promise)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.find({}).toArray().then(res => {
            res.length.should.equal(4);
          });
        });
      });
    });
    
    it('should find many documents by query and return result (promise)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.find({age: 25}).toArray().then(res => {
            res.length.should.equal(2);
          });
        });
      });
    });
    
    it('should find one document and return result (promise)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.findOne({}).then(user => {
            user.name.should.equal('user1');
            user.age.should.equal(25);
          });
        });
      });
    });
    
    it('should find one document by query and return result (promise)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.findOne({age: 29}).then(user => {
            user.name.should.equal('user3');
            user.age.should.equal(29);
          });
        });
      });
    });
  });
  
  describe('Update documents', () => {
    before((done) => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          const user1 = { name: 'user1', age: 25 };
          const user2 = { name: 'user2', age: 22 };
          const user3 = { name: 'user3', age: 29 };
          const user4 = { name: 'user4', age: 25 };
          
          users.insertMany([user1, user2, user3, user4], (err, res) => done());
        });
      });
    });
    
    after((done) => {
      DB.connect('test', (err, db) => {
        db.dropDatabase((err, droped) => done());
      });
    });
    
    it('should update many documents and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.updateMany({age: 25}, {$set: {age: 27}}, (err, res) => {
            res.connection.databaseName.should.equal('test');
            res.matchedCount.should.equal(2);
            res.modifiedCount.should.equal(2);
          });
        });
      });
    });
    
    it('should update one document and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.updateOne({age: 22}, {$set: {age: 23}}, (err, res) => {
            res.connection.databaseName.should.equal('test');
            res.matchedCount.should.equal(1);
            res.modifiedCount.should.equal(1);
          });
        });
      });
    });
    
    it('should upsert document and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.updateMany({age: 24}, {$set: {age: 25}}, {upsert: true}, (err, res) => {
            res.connection.databaseName.should.equal('test');
            res.matchedCount.should.equal(0);
            res.modifiedCount.should.equal(0);
            res.upsertedCount.should.equal(1);
          });
        });
      });
    });
    
    it('should update many documents and return result (promise)', () => {
      DB.connect('test').then(db => {
        db.collection('users', (err, users) => {
          users.updateMany({age: 27}, {$set: {age:26}}).then(res => {
            res.connection.databaseName.should.equal('test');
            res.matchedCount.should.equal(2);
            res.modifiedCount.should.equal(2);
          });
        });
      });
    });
    
    it('should update one document and return result (promise)', () => {
      DB.connect('test').then(db => {
        db.collection('users', (err, users) => {
          users.updateOne({age: 29}, {$set: {age: 28}}).then(res => {
            res.connection.databaseName.should.equal('test');
            res.matchedCount.should.equal(1);
            res.modifiedCount.should.equal(1);
          });
        });
      });
    });
    
    it('should upsert document and return result (promise)', () => {
      DB.connect('test').then(db => {
        db.collection('users', (err, users) => {
          users.updateMany({age: 22}, {$set: {age: 24}}, {upsert: true}).then(res => {
            res.connection.databaseName.should.equal('test');
            res.matchedCount.should.equal(0);
            res.modifiedCount.should.equal(0);
            res.upsertedCount.should.equal(1);
          });
        });
      });
    });
  });
  
  describe('Delete documents', () => {
    beforeEach((done) => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          const user1 = { name: 'user1', age: 25 };
          const user2 = { name: 'user2', age: 22 };
          const user3 = { name: 'user3', age: 29 };
          const user4 = { name: 'user4', age: 25 };
          
          users.insertMany([user1, user2, user3, user4], (err, res) => done());
        });
      });
    });
    
    afterEach((done) => {
      DB.connect('test', (err, db) => {
        db.dropDatabase((err, droped) => done());
      });
    });
    
    it('should delete many documents and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.deleteMany({age: 25}, (err, res) => {
            res.connection.databaseName.should.equal('test');
            res.deletedCount.should.equal(2);
          });
        });
      });
    });
    
    it('should delete one document and return result (callback)', () => {
      DB.connect('test', (err, db) => {
        db.collection('users', (err, users) => {
          users.deleteOne({age: 25}, (err, res) => {
            res.connection.databaseName.should.equal('test');
            res.deletedCount.should.equal(1);
          });
        });
      });
    });
    
    it('should delete many documents and return result (promise)', () => {
      DB.connect('test').then(db => {
        db.collection('users', (err, users) => {
          users.deleteMany({age: 25}).then(res => {
            res.connection.databaseName.should.equal('test');
            res.deletedCount.should.equal(2);
          });
        });
      });
    });
    
    it('should delete one document and return result (promise)', () => {
      DB.connect('test').then(db => {
        db.collection('users', (err, users) => {
          users.deleteOne({age: 25}).then(res => {
            res.connection.databaseName.should.equal('test');
            res.deletedCount.should.equal(1);
          });
        });
      });
    });
  });

});